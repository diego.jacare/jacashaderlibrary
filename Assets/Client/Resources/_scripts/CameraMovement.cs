using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float m_MoveSpeed;
    [SerializeField] private float m_RotateSpeed;
    
    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += m_MoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime * transform.forward;
        
        float angle = Input.GetAxis("Horizontal") * m_RotateSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, angle);
    }
}

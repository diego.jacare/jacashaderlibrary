using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraFrustum : MonoBehaviour
{
    public Camera targetCamera;

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.matrix = inverse(mul(targetCamera.projectionMatrix, targetCamera.worldToCameraMatrix));
        // Gizmos.matrix = inverse(targetCamera.projectionMatrix);
        Gizmos.DrawCube(float3(0, 0, 2), float3(1, 1, 1));
        Gizmos.DrawCube(float3(0, 0, 0), float3(2, 2, 2));
        Gizmos.matrix = Matrix4x4.identity;
    }
#endif
}

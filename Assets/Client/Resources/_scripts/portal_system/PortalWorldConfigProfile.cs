using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PortalWorldConfigProfile")]
public class PortalWorldConfigProfile : ScriptableObject
{
    public PortalWorld[] portalWorlds;
}

[System.Serializable]
public struct PortalWorld
{
    public string name;
    public LayerMask layerMask;
    public int layer;
    public Color skyBoxColor;
}

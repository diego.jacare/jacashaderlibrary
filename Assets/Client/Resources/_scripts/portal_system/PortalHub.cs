using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

public class PortalHub : MonoBehaviour
{

    [SerializeField] PortalWorldConfigProfile portalWorldConfigProfile;
    [SerializeField] PortalCameraController portalCameraController;
    [SerializeField] private GameObject[] dimensions;
    [Tooltip("Use context menu > Populate Portal")] public Portal[] portals;

    void OnEnable()
    {
        EnableAllDimensions();
        PopulateArray();
        ConfigurePortals();
        portalCameraController.Configure(portalWorldConfigProfile);
    }

    private void LateUpdate()
    {
        portalCameraController.UpdatePortalCamera(portalWorldConfigProfile);
        foreach(Portal portal in portals)
        {
            portal.UpdateCamera(portalCameraController.CurrentWorld(), portalCameraController.transform.position, portalCameraController.transform.rotation, portalCameraController.GetMainFov(), portalWorldConfigProfile, portalCameraController.GetMainResolution());
        }
    }

    public int QueryVisiblePortals(Portal[] portalsOutput)
    {
        int count = 0;
        int maxCount = max(portals.Length, portalsOutput.Length);

        for(int i = 0; i < maxCount; i++)
        {
            if (portals[i].IsVisible(portalCameraController.CurrentWorld()))
            {
                portalsOutput[count] = portals[i];
                count++;
            } 
        }
        return count + 1;
    }

    [ContextMenu("Populate Portal")]
    public void PopulateArray()
    {
        portals = FindObjectsOfType<Portal>();
    }

    public PortalWorldConfigProfile GetProfile()
    {
        return portalWorldConfigProfile;
    }

    void ConfigurePortals()
    {
        foreach (Portal portal in portals)
        {
            portal.Configure(portalWorldConfigProfile);
        }
    }

    void EnableAllDimensions()
    {
        foreach (GameObject dimension in dimensions)
        {
            dimension.SetActive(true);
        }
    }
}

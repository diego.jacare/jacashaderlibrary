using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;


public class PortalCameraController : MonoBehaviour
{

    [SerializeField] Camera main;
    [SerializeField] int startWorld;

    float3 lastCamPosition;
    int currentWorld;

    public void Configure(PortalWorldConfigProfile profile)
    {
        EnterWorld(startWorld, profile);
        lastCamPosition = main.transform.position;
    }

    void EnterWorld(int world, PortalWorldConfigProfile profile)
    {
        currentWorld = world;
        main.cullingMask = profile.portalWorlds[world].layerMask;
        main.backgroundColor = profile.portalWorlds[world].skyBoxColor;
    }

    public void UpdatePortalCamera(PortalWorldConfigProfile profile)
    {
        RaycastHit raycastHit;
        float3 currentPosition = main.transform.position;
        float3 deltaPosition = currentPosition - lastCamPosition;

        if (Physics.Raycast(lastCamPosition, deltaPosition, out raycastHit, length(deltaPosition)))
        {
            Portal portalComponent = raycastHit.collider.GetComponent<Portal>();
            if (portalComponent)
            {
                float DdotF = dot(deltaPosition, portalComponent.transform.forward);
                
                int worldToLeave;
                worldToLeave = DdotF < 0 ? portalComponent.worldForward : portalComponent.worldBackward;

                if(currentWorld == worldToLeave)
                {
                    int worldToEnter;
                    worldToEnter = DdotF >= 0 ? portalComponent.worldForward : portalComponent.worldBackward;
                    EnterWorld(worldToEnter, profile);
                }                
            }
        }

        lastCamPosition = currentPosition;
    }

    public int CurrentWorld()
    {
        return currentWorld;
    }

    public float GetMainFov()
    {
        return main.fieldOfView;
    }

    public int2 GetMainResolution()
    {
        return new int2(main.pixelWidth, main.pixelHeight);
    }
}

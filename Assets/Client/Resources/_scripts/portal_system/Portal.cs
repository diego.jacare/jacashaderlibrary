using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;


public class Portal : MonoBehaviour
{
    public int worldForward;
    public int worldBackward;

    [SerializeField] GameObject portalForward;
    [SerializeField] GameObject portalBackward;
    [SerializeField] Camera portalCamera;
    [SerializeField] Material portalMaterial;

    Material _material;
    RenderTexture _renderTexture;
    RenderTextureDescriptor descriptor;


    float3 position;
    float3 forward;

    private void OnEnable()
    {
        descriptor = new RenderTextureDescriptor(512, 512, RenderTextureFormat.ARGB32, 16, 0);
        descriptor.useDynamicScale = true;
        _renderTexture = new RenderTexture(descriptor);
        portalCamera.targetTexture = _renderTexture;
        _material = new Material(portalMaterial);
        _material.SetTexture("_MainTex", _renderTexture);
        portalForward.GetComponent<MeshRenderer>().sharedMaterial = _material;
        portalBackward.GetComponent<MeshRenderer>().sharedMaterial = _material;
    }

    private void OnDisable()
    {
        _renderTexture.Release();
        Destroy(_renderTexture);
    }

    public bool IsVisible(int currentWorld) 
    {
        return currentWorld == worldForward || currentWorld == worldBackward;
    }

    public void Configure(PortalWorldConfigProfile profile)
    {
        position = transform.position;
        forward = transform.forward;
        portalForward.layer = profile.portalWorlds[worldForward].layer;
        portalBackward.layer = profile.portalWorlds[worldBackward].layer;
    }

    public void UpdateCamera(int currentWorld, float3 camPosition, quaternion camRotation, float fov, PortalWorldConfigProfile profile, int2 targetResolution)
    {
        if (IsVisible(currentWorld))
        {
            float3 portalCenter = transform.position;
            portalCamera.enabled = true;
            portalCamera.transform.position = camPosition;
            portalCamera.transform.rotation = camRotation;
            portalCamera.fieldOfView = fov;
            float nearDistance = dot(portalCenter - camPosition, forward(camRotation));
            portalCamera.nearClipPlane = nearDistance;
            portalCamera.backgroundColor = profile.portalWorlds[OtherWorld(currentWorld)].skyBoxColor;
            portalCamera.cullingMask = profile.portalWorlds[OtherWorld(currentWorld)].layerMask;
            UpdateTextureResolution(targetResolution);
            
            // TODO: Improve to generate obliqueMatrix to near clip plane
            // float4x4 worldToCamera = portalCamera.transform.worldToLocalMatrix;
            // float3 portalWorldNormal = - PortalNormal(currentWorld);
            // float3 planeNormal = - mul(worldToCamera, float4(portalWorldNormal, 0)).xyz;
            // float planeDistance = - dot( portalCenter + camPosition, planeNormal);
            // float4x4 obliqueMatrix = portalCamera.CalculateObliqueMatrix(float4(planeNormal, planeDistance));
            // portalCamera.projectionMatrix = obliqueMatrix;
        }
        else
        {
            portalCamera.enabled = false;
        }
    }

    void UpdateTextureResolution(int2 resolution)
    {
        if(resolution.x != _renderTexture.width || resolution.y != _renderTexture.height)
        {
            _renderTexture.Release();
            Destroy(_renderTexture);
            descriptor.width = resolution.x;
            descriptor.height = resolution.y;
            _renderTexture = new RenderTexture(descriptor);
            portalCamera.targetTexture = _renderTexture;
            _material.SetTexture("_MainTex", _renderTexture);
        }
    }

    int OtherWorld(int currentWorld)
    {
        return currentWorld == worldForward ? worldBackward : worldForward;
    }

    float3 PortalNormal(int currentWorld)
    {
        if (currentWorld == worldForward)
        {
            return portalForward.transform.forward;
        }
        else if (currentWorld == worldBackward)
        {
            return -portalForward.transform.forward;
        }
        else
        {
            return float3(0, 1, 0);
        }

    }
}

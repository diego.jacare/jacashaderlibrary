using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubblesBurst : MonoBehaviour
{
    [SerializeField] private Material m_bubbleMaterial;
    [SerializeField] private float m_burstSpeed;
    [SerializeField] private float m_cooldown;
    [SerializeField] private float m_max_range;
    private bool m_isBursting;
    private float m_radius;

    private void Awake()
    {
        m_radius = 0.0f;
        m_bubbleMaterial.SetFloat("_waveRadius", m_radius);
        m_isBursting = false;
    }

    void FixedUpdate()
    {
        
        RaycastHit hit; 
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
        if ( Physics.Raycast (ray,out hit,200.0f) && !m_isBursting)
        {
            StartCoroutine(BurstBubble(hit.point));
        }
        
        if (m_isBursting && m_radius <= m_max_range)
        {
            m_bubbleMaterial.SetFloat("_waveRadius", m_radius);
            m_radius += m_burstSpeed;
        }
    }

    IEnumerator BurstBubble(Vector3 hitPosition)
    {
        m_bubbleMaterial.SetVector("_epicenter", hitPosition);
        m_isBursting = true;
        yield return new WaitForSeconds(m_cooldown);
        m_isBursting = false;
        m_radius = 0.0f;
        m_bubbleMaterial.SetFloat("_waveRadius", m_radius);
    }
}

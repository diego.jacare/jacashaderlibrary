Shader "JacaShaders/Lighting/SimplePBR"
{
    Properties
    {
        _Color("Albedo", color) = (1,1,1,1)
        _metallic("Metallic", range(0,1)) = 0
        _roughness("Roughness", range(0,1)) = 1
        _f0("Fresnel Reflectance at 0 Degrees", range(0,1)) = 0.045
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull", float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "RenderPipeline"="UniversalPipeline"}
        
        HLSLINCLUDE 
            #pragma vertex vert
            #pragma fragment frag
            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"
            #include "Assets/Client/Resources/_shaders/libraries/jaca_lights_lib.hlsl"

            CBUFFER_START(UnityPerMature)
            half4 _Color;
            half _metallic;
            half _roughness;
            half _f0;
            CBUFFER_END
        ENDHLSL

        Pass
        {
            Cull[_Cull]
            ZWrite On
            HLSLPROGRAM

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float3 normal : NORMAL;
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD0;
            };


            v2f vert (appdata v)
            {   
                v2f o;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex);
                VertexNormalInputs vertexNormal = GetVertexNormalInputs(v.normal);
                o.vertex = vertexInput.positionCS;
                o.worldPos = vertexInput.positionWS;
                o.normal = v.normal;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Surface
                float3 albedo = _Color.xyz;
                float roughness = _roughness * _roughness;

                // Geometry
                GeometryData geometryData;
                GetGeometryData(i.normal, i.worldPos, geometryData );
                
                // Main Light
                CustomLight customLight = GetCustomMainLight(geometryData);

                // Global Illumination
                GlobalIlluminationData globalIlluminationData = GetGlobalIlluminationData(geometryData);

                // BRDF
                BRDFInfo brdfData;
                GetBRDFInfo(albedo, roughness, _metallic, _f0, brdfData);

                // Render
                float3 render = RenderLight(brdfData, geometryData, customLight);
                render += RenderGlobalIllumination(brdfData, geometryData, globalIlluminationData);;
                return half4(render, 1);
            }
            ENDHLSL
        }
    }
}

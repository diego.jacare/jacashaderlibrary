Shader "JacaShaders/Effects/XRay"
{
    Properties
    {
        
        _colorBase ("Base Color", Color) = (1,1,1,1)
        _MainTex ("Base Texture", 2D) = "white" {}
        
        _xrayColorA ("X-Ray Color A", Color) = (1,1,1,1)
        _xrayColorB ("X-Ray Color B", Color) = (1,1,1,1)
        _xraySlices ("X-Ray Slices", Range(3, 40)) = 1
        _xraySpeed ("X-Ray Speed", Range(-1, 1)) = 0
        _xrayRimLevels("X-Ray Rim Levels", Vector) = (0,1,0,1)

    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
            "RenderPipeline"="UniversalPipeline"
        }

        Pass
        {
            Blend One One
            ZTest GEqual
            ZWrite Off

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"

            uniform half4 _xrayColorA;
            uniform half4 _xrayColorB;
            uniform half _xraySlices;
            uniform half _xraySpeed;
            uniform half4 _xrayRimLevels;


            struct appdata
            {
                float4 vertex : POSITION;
                float3 objectNormal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                half4 vertex : SV_POSITION;
                half3 worldNormal : NORMAL;
                half2 uv : TEXCOORD0;
                half3 worldPosition : TEXCOORD1;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.worldNormal = normalize(mul(unity_ObjectToWorld, half4(v.objectNormal, 0)).xyz);
                o.worldPosition = mul(unity_ObjectToWorld, v.vertex).xyz;
                o.uv = v.uv;
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                // Stripes effect
                half stripe_movement = (-i.vertex.y * 0.005 + (_Time.y * _xraySpeed));
                half pattern = tan(stripe_movement * PI * _xraySlices);

                // Rim
                half3 normalDir = normalize(i.worldNormal);
                half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPosition);
                half NdotV = dot(normalDir, viewDir);
                half rim = 1 - (2 * abs(Levels(NdotV, _xrayRimLevels) - .5));

                // Mask
                half mask = saturate(rim + pattern);
                half4 color = lerp(_xrayColorA, _xrayColorB, mask);

                return color;
            }
            ENDHLSL
        }

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"

            uniform half4 _colorBase;
            uniform sampler2D _MainTex;
            uniform half4 _MainTex_ST;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 objectNormal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                half4 vertex : SV_POSITION;
                half3 worldNormal : NORMAL;
                half2 uv : TEXCOORD0;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.worldNormal = normalize(mul(unity_ObjectToWorld, half4(v.objectNormal, 0)).xyz);
                o.uv = v.uv;
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                // Albedo
                half3 albedo = tex2D(_MainTex, i.uv).xyz * _colorBase.xyz;

                return half4(albedo,1);
            }
            ENDHLSL
        }
    }
}
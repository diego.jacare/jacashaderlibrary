Shader "JacaShaders/Effects/Melting"
{
    Properties
    {
        _colorA ("Color A", Color) = (1,1,1,1)
        _colorB ("Color B", Color) = (1,1,1,1)
        _scale ("Scale", Float) = 1
        _offset ("Offset", Float) = 0
        _slices_x ("Slices On X", Range(1, 20)) = 1
        _slices_y ("Slices On Y", Range(1, 20)) = 1
        _slice_gradient ("Slice Gradient", Range(-2, 2)) = 0.5
        _speed ("Speed", Range(-2, 2)) = 0
        
    }
    SubShader
    {
        Tags { 
            "RenderType"="Transparent" 
            "Queue"="Transparent" 
        }

        Pass
        {
            Blend One One
            ZWrite Off
            ZTest LEqual //default
            Cull Back //default 
            
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"

            uniform half4 _colorB;
            uniform half4 _colorA;
            uniform half _scale;
            uniform half _offset;
            uniform half _slices_x;
            uniform half _slices_y;
            uniform half _slice_gradient;
            uniform half _speed;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 objectNormal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                half4 vertex : SV_POSITION;
                half3 worldNormal : NORMAL;
                half2 uv : TEXCOORD0;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.worldNormal = normalize(mul(unity_ObjectToWorld, half4(v.objectNormal, 0)).xyz);
                o.uv = (v.uv + _offset) * _scale ;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Albedo
                half4 gradient =  lerp(_colorA, _colorB, i.uv.y);

                // Effects
                half waves = cos(i.uv.x * TAU * _slices_x) * (1 - i.uv.y) * 0.1;
                half waves_movement = (-i.uv.y  + waves + (_Time.y * _speed));
                half pattern = tan( waves_movement * TAU * _slices_y) * _slice_gradient + 0.5; // This * shifts the range
                half iverse_pattern = pattern * (1 - i.uv.y);

                // Mesh Handling
                half remove_bottom_top = 1 - abs(dot(i.worldNormal.y, 1));

                return gradient * saturate(iverse_pattern) * saturate(remove_bottom_top);
            }
            ENDHLSL
        }
    }
}

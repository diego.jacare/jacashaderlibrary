Shader "JacaShaders/Effects/Wave"
{
    Properties
    {
        _colorA ("Color A", Color) = (1,1,1,1)
        _colorB ("Color B", Color) = (1,1,1,1)
        _wave_height ("Wave Height", Range(0,2)) = 1
        _waves_amount ("Waves Amount", Range(0, 10)) = 3
        _speed ("Speed", Range(-2, 2)) = 0
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque"}

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"

            uniform half4 _colorB;
            uniform half4 _colorA;
            uniform half _wave_height;
            uniform half _waves_amount
   ;
            float _speed;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 objectNormal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                half4 vertex : SV_POSITION;
                half3 worldNormal : NORMAL;
                half2 uv : TEXCOORD0;
            };

            v2f vert (appdata v)
            {
                v2f o;
                v.vertex.y = RadialWaveWithGradient(v.uv, _waves_amount, _speed) * _wave_height;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.worldNormal = normalize(mul(unity_ObjectToWorld, half4(v.objectNormal, 0)).xyz);
                o.uv = v.uv;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Albedo
                half4 gradient =  lerp(_colorA, _colorB, RadialWaveWithGradient(i.uv, _waves_amount, _speed));
                return gradient;
            }
            ENDHLSL
        }
    }
}

Shader "JacaShaders/Effects/PortalShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _outline ("Outline", Range(0,1)) = 0.2
        _outlineColor("Outline Color", Color) = (1,1,1,0)
        _outlineTex ("Outline Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "RenderPipeline"="UniversalPipeline"}

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

            uniform sampler2D _MainTex;
            uniform half4 _MainTex_ST;
            uniform sampler2D _outlineTex;
            uniform half4 _outlineTex_ST;
            uniform half _outline;
            uniform half4 _outlineColor;


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv2 : TEXCOORD0;
            };

            struct v2f
            {
                half4 uv : TEXCOORD0;
                half2 uv2 : TEXCOORD1;
                half4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;                
                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex);
                o.vertex = vertexInput.positionCS;
                o.uv = ComputeScreenPos(vertexInput.positionCS);
                o.uv2 = v.uv2; 
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Noise
                half4 border = tex2D(_outlineTex, i.uv2 + _Time.y * _outlineTex_ST.zw);
                
                // Rounded borders
                half2 uniformed_coord = float2(i.uv2.x * 1.25, i.uv2.y);
                half2 point_on_line_segment = float2(clamp(uniformed_coord.x, 0.5, 0.75), 0.5);
                half sined_distance = (distance(uniformed_coord, point_on_line_segment)* 2 - 1) * border.w;
                clip(-sined_distance);

                // Outline border
                half mask = step(0, (sined_distance + _outline)) * border.w;
                
                
                // sample the texture
                half4 col = tex2D(_MainTex, i.uv.xy/i.uv.w);
                half4 output_color = lerp(col,  _outlineColor, mask);
                    
                return output_color;
            }
            ENDHLSL
        }
    }
}

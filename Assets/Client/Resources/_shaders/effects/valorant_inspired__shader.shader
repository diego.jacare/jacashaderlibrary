Shader "JacaShaders/Effects/ValontInspired"
{
    Properties
    {
        _MainTex ("Layer 1", 2D) = "white" {}
        _layer2 ("Layer 2", 2D) = "white" {}
        _gradient("Gradient", 2D) = "white" {}
        _cubeMap("Cube Map", Cube) = "white" {}
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull", float) = 0
        _offset("Offset", range(0, 5)) = 0
        _fresnelWeight("Fresnel Weight", range(0, 1)) = 0
        _outerFresnel("Outer Fresnel", range(0, 1)) = 0
        _outerPattern("Outer Pattern", range(0, 1)) = 0
        _smoothness("Smoothness", range(0, 1)) = 0
        _starSize("Star Size", range(0, 1)) = 0
        _depthLimit("Depth Limit", range(0.001, 1)) = 0.5
        _Color("Color Base", Color) = (1,1,1,1)
        _color2("Color 2", Color) = (1,1,1,1)
        [HDR] _starColor("Star Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "RenderPipeline"="UniversalPipeline"}
        
        HLSLINCLUDE 
            #pragma vertex vert
            #pragma fragment frag
            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"

            CBUFFER_START(UnityPerMature)
                uniform sampler2D _MainTex;
                uniform float4 _MainTex_ST;
                uniform sampler2D _layer2;
                uniform float4 _layer2_ST;
                uniform sampler2D _gradient;
                uniform samplerCUBE _cubeMap;
                uniform float _offset;
                uniform float _fresnelWeight;
                uniform float _outerFresnel;
                uniform float _outerPattern;
                uniform float _smoothness;
                uniform float _starSize;
                uniform float _depthLimit;
                uniform float4 _Color;
                uniform float4 _color2;
                uniform float4 _color3;
                uniform float4 _starColor;
            CBUFFER_END
        ENDHLSL

        Pass
        {
            Cull[_Cull]
            ZWrite On
            HLSLPROGRAM

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 worldPosition : TEXCOORD1;
                float3 normal : NORMAL;
                float4 screenPosition : TEXCOORD2;
            };

            v2f vert (appdata v)
            {
                v2f o;
                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex);
                VertexNormalInputs vertexNormal = GetVertexNormalInputs(v.normal, v.tangent);
                o.vertex = vertexInput.positionCS;
                o.worldPosition.xyz = vertexInput.positionWS;
                o.worldPosition.w = -vertexInput.positionVS.z;
                float3 view = normalize(_WorldSpaceCameraPos - vertexInput.positionWS);
                o.normal = vertexNormal.normalWS;
                o.uv.xy = NormalProjectedUV(v.uv * _MainTex_ST.xy + _MainTex_ST.zw * _Time.y, vertexNormal.normalWS, view, _offset, vertexNormal.tangentWS, vertexNormal.bitangentWS);
                o.uv.wz = NormalProjectedUV(v.uv * _layer2_ST.xy + _layer2_ST.zw * _Time.y, vertexNormal.normalWS, view, 0, vertexNormal.tangentWS, vertexNormal.bitangentWS);
                o.screenPosition = ComputeScreenPos(vertexInput.positionCS);
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
               //return Noise(i.uv.xy);
                float backgroundDepth = SampleLinearDepth(i.screenPosition.xy / i.screenPosition.w);
                float pixelDepth = i.worldPosition.w;
                float depthGradient = (saturate(backgroundDepth - pixelDepth)/ _depthLimit);
                
                // sample the texture
                half layer1 = tex2D(_MainTex, i.uv.xy).w;
                half layer2 = tex2D(_layer2, i.uv.zw).w;

                half3 view = normalize(_WorldSpaceCameraPos - i.worldPosition);
                half4 cubeMap = texCUBE(_cubeMap, -view);

                half3 normal = normalize(i.normal);

                half NdotV = dot(normal, view);
                half fresnel = NdotV * NdotV;

                half outlineGradient = (fresnel * depthGradient);
                
                half starMask = smoothstep(_starSize - _smoothness, _starSize + _smoothness, cubeMap.w);
                half maskLayer2 = (1 + outlineGradient * (layer2 * _outerPattern - 1)) * (1 - _outerFresnel) + _outerFresnel;
                half maskLayer1 = smoothstep(0.5 - _smoothness, 0.5 + _smoothness, lerp(layer1, 1 - outlineGradient, _fresnelWeight));
                
                half4 gradient = tex2D(_gradient, half2(1 - maskLayer2, 0));
                
                half4 col = lerp(_Color, _starColor, starMask);
                col = lerp(col, _color2, maskLayer1);
                col = lerp(col, gradient, gradient.w);
                return col;
            }
            ENDHLSL
        }
    }
}

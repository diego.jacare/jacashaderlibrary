Shader "JacaShaders/UI/HealthBar"
{
    Properties
    {
        _fullColor("Full Life Color", Color) = (0,1,0,0)
        _emptyColor("Empty Life Color", Color) = (1,0,0,0)
        _minLifeThreshold ("Min Life Color Threshold", float) = 0.2
        _maxLifeThreshold ("Max Life Color Threshold", float) = 0.8
        _outlineColor("Outline Color", Color) = (1,1,1,0)
        _outline ("Outline", Range(0,1)) = 0.2
        _pulse ("Pulse At", float) = 0.2
        _speed ("Pulse Speed", Range(0,10)) = 5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"
            
            uniform half4 _fullColor;
            uniform half4 _emptyColor;
            uniform half4 _outlineColor;
            uniform half _minLifeThreshold;
            uniform half _maxLifeThreshold;
            uniform half _pulse;
            uniform half _speed;
            uniform half _outline;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 vertexColor : COLOR;
            };

            struct v2f
            {
                half2 uv : TEXCOORD0;
                half4 vertex : SV_POSITION;
                half4 vertexColor : COLOR;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = TransformObjectToHClip(v.vertex);
                o.uv = v.uv;
                o.vertexColor = v.vertexColor;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Rounded borders
                half2 uniformed_coord = float2(i.uv.x * 8, i.uv.y);
                half2 point_on_line_segment = float2(clamp(uniformed_coord.x, 0.5, 7.5), 0.5);
                half sined_distance = (distance(uniformed_coord, point_on_line_segment)* 2 - 1);
                clip(-sined_distance);
                half border = step(0, (sined_distance + _outline));

                // Mask
                half mask = step(i.uv.x, i.vertexColor.x);
                clip(mask - 0.1 + border);

                // Albedo
                half threshold = InverseLerpClamped(_minLifeThreshold, _maxLifeThreshold, i.vertexColor.x);
                half4 col = lerp(_emptyColor, _fullColor, threshold);

                // Gradient
                col *= float4(i.uv.yyy -.1 ,0);

                // Pulse Effect
                half can_pulse = step(i.vertexColor.x, _pulse);
                half amplitude = 0.5;
                half pulse_effect = cos(_Time.y * _speed) * amplitude + 1;
                pulse_effect = lerp(1, pulse_effect, can_pulse);
                
                half4 output_color = col * pulse_effect;
                output_color = lerp(output_color,  _outlineColor, border);
                    
                return output_color;
            }
            ENDHLSL
        }
    }
}

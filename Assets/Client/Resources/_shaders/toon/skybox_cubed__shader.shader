﻿Shader "JacaShaders/Cartoon/SkyboxCubed"
{
    Properties
    {
        _Tint ("Tint Color  1", Color) = (.5, .5, .5, .5)
        _Tint2 ("Tint Color 2", Color) = (.5, .5, .5, .5)
        [Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
        _Rotation ("Rotation", Range(0, 360)) = 0
        _steps ("Steps", Range (1, 40)) = 5
        [NoScaleOffset] _Tex ("Cubemap (HDR)", Cube) = "grey" {}

    }

    SubShader
    {
        Tags
        {
            "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox"
        }
        Cull Off ZWrite Off

        Pass
        {

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile _ _SIMPLEFOG

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"

            uniform samplerCUBE _Tex;
            uniform half4 _Tex_HDR;
            uniform half4 _Tint;
            uniform half4 _Tint2;
            uniform half _Exposure;
            uniform float _Rotation;
            uniform half _steps;

            float3 RotateAroundYInDegrees(float3 vertex, float degrees)
            {
                float alpha = degrees * PI / 180.0;
                float sina, cosa;
                sincos(alpha, sina, cosa);
                float2x2 m = float2x2(cosa, -sina, sina, cosa);
                return float3(mul(m, vertex.xz), vertex.y).xzy;
            }

            struct appdata_t
            {
                float4 vertex : POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                half4 vertex : SV_POSITION;
                half3 texcoord : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2f vert(appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                half3 rotated = RotateAroundYInDegrees(v.vertex, _Rotation);
                o.vertex = TransformObjectToHClip(rotated);
                o.texcoord = v.vertex.xyz;
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                half4 tex = texCUBE(_Tex, i.texcoord);
                half noise = tex.w;
                noise = Posterize(noise, _steps);
                half3 color = lerp(_Tint.xyz, _Tint2.xyz, noise) * _Exposure;
                return half4(color, 1);
            }
            ENDHLSL
        }
    }
    Fallback Off
}
﻿Shader "JacaShaders/Cartoon/Bacterium"
{
    Properties
    {
        _baseTex ("Base Texture", 2D) = "white" {}
        _color1 ("Tint", Color) = (1,1,1,1)

        // Bubbles
        [Toggle(_BUBBLES)] _bubbles ("Bubbles Enabled", float) = 0
        _planeProjectedTexture ("Bubbles Plane Projected Texture", 2D) = "white" {}
        _bubbleObliqueOclusion ("Bubble Oblique Oclusion", range (0,1)) = 0.5
        _bubbleThreshold ("Bubble Threshold", range (0,1)) = 0.5

        // Noises
        [NoScaleOffset] _MainTex ("Noise 1", 2D) = "white" {}
        _triplanarTiling("Triplanar Noise 1", Vector) = (1,1,1,1)
        _triplanarSpeed("Triplanar Noise 1", Vector) = (0,0,0,0)
        [NoScaleOffset] _MainTex2 ("Noise 2", 2D) = "white" {}
        _triplanarTiling2("Triplanar Noise 2", Vector) = (1,1,1,1)
        _triplanarSpeed2("Triplanar Noise 2", Vector) = (0,0,0,0)
        
        // Outline
        _threshold ("Outline Threshold", range (0,1)) = 0.5
        _smoothness("Outline Smoothness", range (0,1)) = 0
        _color2 ("Outiline Color", Color) = (0,0,0,1)
        _viewComponent ("Outline View Component", range (0, 1)) = 0.5
        _noiseBalance ("Outline Noise Balance between 1 and 2", range (0, 1)) = 0.5

        // Vertex Offset
        _offsetIntensity("Vertex Offset Intensity", range (0, 5)) = 0
        
        // Lighting
         _diffuseSize("Diffuse Size", range (0,1)) = 0
         _subsurfaceSize("Subsurface Size", range (0,1)) = 0.5
         _subsurfaceIntensity("Subsurface Intensity", range (0,1)) = 0
         _ambientSubsurfaceIntensity("Ambient Subsurface Intensity", range (0,1)) = 0
         _specularLevels("Specular Levels", Vector) = (0,1,0,1)
         _specularLengthLevels("Specular Lenght Levels", Vector) = (0,1,0,1)

        // Hit Wave
        _epicenter("Wave Center", Vector) = (0,0,0,0)
        _waveRadius("Wave Radius", Float) = 3
        _waveWidth("Wave Width", Float) = 1
        _waveHeight("Wave Height", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            Tags { "LightMode"="UniversalForward" }

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma shader_feature _ _BUBBLES

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"
            
            uniform sampler2D _baseTex;
            uniform sampler2D _planeProjectedTexture;
            uniform half4 _planeProjectedTexture_ST;
            uniform sampler2D _MainTex;
            uniform half4 _MainTex_ST;
            uniform sampler2D _MainTex2;
            uniform half4 _MainTex2_ST;
            uniform half _threshold;
            uniform half _smoothness;
            uniform half4 _color1;
            uniform half4 _color2;
            uniform half4 _triplanarTiling;
            uniform half4 _triplanarSpeed;
            uniform half4 _triplanarTiling2;
            uniform half4 _triplanarSpeed2;
            uniform half _offsetIntensity;
            uniform half _diffuseSize;
            uniform half _viewComponent;
            uniform half _noiseBalance;
            uniform half _subsurfaceIntensity;
            uniform half _ambientSubsurfaceIntensity;
            uniform half _subsurfaceSize;
            uniform half4 _specularLevels;
            uniform half4 _specularLengthLevels;
            uniform half4 _epicenter;
            uniform half _waveRadius;
            uniform half _waveWidth;
            uniform half _waveHeight;
            uniform half _bubbleObliqueOclusion;
            uniform half _bubbleThreshold;

            struct appdata
            {
                float4 objectPosition : POSITION;
                float2 uv : TEXCOORD0;
                float3 objectNormal : NORMAL;

            };

            struct v2f
            {
                half2 uv : TEXCOORD0;
                half4 clipPosition : SV_POSITION;
                half3 worldNormal : NORMAL;
                half3 worldPosition : TEXCOORD2;
                half3 triplanarCoord : TEXCOORD3;
                half3 triplanarCoord2 : TEXCOORD4;
                #if _BUBBLES
                half2 planeProjectedUV : TEXCOORD5;
                #endif
            };

            
            v2f vert (appdata v)
            {
                v2f o;
                o.worldPosition = mul(unity_ObjectToWorld, v.objectPosition).xyz;

                #if _BUBBLES
                half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - o.worldPosition);
                o.planeProjectedUV = PlaneProjectedUV(v.objectPosition, viewDir);
                o.planeProjectedUV = o.planeProjectedUV * _planeProjectedTexture_ST.xy + _planeProjectedTexture_ST.zw * _Time.y;
                #endif

                o.uv = v.uv;
                o.worldNormal = normalize(mul(unity_ObjectToWorld, half4(v.objectNormal, 0)).xyz);
                o.triplanarCoord = v.objectPosition.xyz * _triplanarTiling.xyz + _triplanarSpeed.xyz * _Time.y;
                o.triplanarCoord2 = v.objectPosition.xyz * _triplanarTiling2.xyz + _triplanarSpeed2.xyz * _Time.y;

                half noise = TriplanarLod(_MainTex, o.triplanarCoord, o.worldNormal, 1);
                half dist = length(o.worldPosition - _epicenter.xyz);
                half wave = SmoothWave(dist, _waveWidth, _waveHeight, _waveRadius);
                o.worldPosition = o.worldPosition + o.worldNormal * _offsetIntensity * (wave + noise);
                o.clipPosition = mul(UNITY_MATRIX_VP, half4(o.worldPosition, 1));

                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Samples
                half3 textureColor = tex2D(_baseTex, i.uv).xyz * _color1.xyz;
                half noise1 = Triplanar(_MainTex, i.triplanarCoord, i.worldNormal);
                half noise2 = Triplanar(_MainTex2, i.triplanarCoord2, i.worldNormal);
                #if _BUBBLES
                half planeProjectedTexture = tex2D(_planeProjectedTexture, i.planeProjectedUV).w;
                #endif

                // Geometry
                half3 normalDir = normalize(i.worldNormal);
                half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPosition);
                half NdotV = dot(normalDir, viewDir);
                  // noise = step(noise, _threshold);
                half dist = length(i.worldPosition - _epicenter.xyz);
                half wave = SmoothWave(dist, _waveWidth, _waveHeight, _waveRadius);
                
                // Surface
                half noiseCombine = lerp(noise1, noise2, _noiseBalance);
                noiseCombine = lerp(noiseCombine, 1 - NdotV + wave, _viewComponent);
                half noiseFinal = smoothstep(_threshold - _smoothness, _threshold + _smoothness, noiseCombine);
                
                #if _BUBBLES
                half bubblesCombined = step(_bubbleThreshold, lerp(planeProjectedTexture, NdotV, _bubbleObliqueOclusion));
                textureColor = lerp(textureColor, _color2.xyz, bubblesCombined);
                #endif
                half3 albedo = lerp(textureColor, _color2.xyz, noiseFinal);

                // Light 0
                Light light0 = GetMainLight();
                half3 halfDir = normalize(viewDir + light0.direction);
                half NdotL = saturate(dot(normalDir, light0.direction));
                half diffuseLobe = step(_diffuseSize, NdotL);
                half NdotH = dot(normalDir, halfDir);
                half specularLobe = Levels(NdotL, _specularLengthLevels);
                half3 subsurfaceLobe = step(_subsurfaceSize, NdotH * NdotV) * light0.color;
                half specularRim = 1- (2 * abs(Levels(NdotV, _specularLevels) - .5));
                specularLobe = step(.2, specularLobe * specularRim);
                half3 light0Intensity = diffuseLobe * light0.color * light0.shadowAttenuation;


                // Global Illumination
                half3 globalIlluminationIntensity = SampleSHVertex(normalDir);

                // Lighting
                half3 render = albedo * light0Intensity; // Direct diffuse
                render += light0Intensity * specularLobe; // Direct specular
                render += albedo * subsurfaceLobe * _subsurfaceIntensity; //Direct subsurface
                render += albedo * globalIlluminationIntensity; // Ambient diffuse
                render += albedo * globalIlluminationIntensity * _ambientSubsurfaceIntensity; // Ambient subsurface

                return half4 (render, 1);
            }
            ENDHLSL
        }
    }
}

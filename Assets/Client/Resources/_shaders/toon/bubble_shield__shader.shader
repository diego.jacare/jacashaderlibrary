﻿Shader "JacaShaders/Cartoon/BubbleShield"
{
    Properties
    {
        
        [NoScaleOffset] _MainTex ("Noise", 2D) = "white" {}
        _triplanarTiling("Noise Triplanar Tiling", Vector) = (1,1,1,1)
        _triplanarSpeed("Noise Triplanar Speed", Vector) = (0,0,0,0)

        [NoScaleOffset] _burstTex ("Burst", 2D) = "white" {}
        _triplanarTiling2 ("Burst Tiling", Vector) = (1,1,1,1)
        _triplanarSpeed2 ("Burst Speed", Vector) = (0,0,0,0)
        _burstDelay ("Burst Delay", Float) = 10
        _burstWidth("Burst Width", Float) = 1

        _offsetIntensity("Vertex Offset Intensity", range (0, 5)) = 0
        _mainColor ("Color", Color) = (0,0,0,0)

        _epicenter("Wave Center", Vector) = (0,0,0,0)
        _waveRadius("Wave Radius", Float) = 3
        _waveWidth("Wave Width", Float) = 1
        _waveHeight("Wave Height", Float) = 1

        _specularLengthLevels("Specular Levels", Vector) = (0,1,0,1)
        _rimLevels("Rim Levels", Vector) = (0,1,0,1)
  
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="True" "DisableBatching"="True"}

        Pass
        {
            Tags { "LightMode"="UniversalForward" }
            ZWrite Off
            Blend One One
            Cull off

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"
            
            uniform sampler2D _MainTex;
            uniform half4 _MainTex_ST;
            uniform sampler2D _burstTex;
            uniform half4 _burstTex_ST;
            uniform half4 _mainColor;
            uniform half _offsetIntensity;
            uniform half4 _triplanarTiling;
            uniform half4 _triplanarSpeed;
            uniform half4 _triplanarTiling2;
            uniform half4 _triplanarSpeed2;
            //wave
            uniform half4 _epicenter;
            uniform half _waveRadius;
            uniform half _waveWidth;
            uniform half _waveHeight;
            uniform half _burstDelay;
            uniform half _burstWidth;
            
            uniform half4 _specularLengthLevels;
            uniform half4 _rimLevels;


            struct appdata
            {
                float4 objectPosition : POSITION;
                float3 objectNormal : NORMAL;

            };

            struct v2f
            {
                half4 clipPosition : SV_POSITION;
                half3 worldNormal : NORMAL;
                half3 worldPosition : TEXCOORD2;
                half3 triplanarCoord2 : TEXCOORD4;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                
                o.worldPosition = mul(unity_ObjectToWorld, v.objectPosition).xyz;
                o.worldNormal = normalize(mul(unity_ObjectToWorld, half4(v.objectNormal, 0)).xyz);
                o.triplanarCoord2 = o.worldPosition * _triplanarTiling2.xyz + _triplanarSpeed2.xyz * _Time.y;
                
                half3 triplanarCoord = o.worldPosition * _triplanarTiling.xyz + _triplanarSpeed.xyz * _Time.y;
                half noise = TriplanarLod(_MainTex, triplanarCoord, o.worldNormal, 1);

                half dist = length(o.worldPosition - _epicenter.xyz);
                half wave = SmoothWave(dist, _waveWidth, _waveHeight, _waveRadius);
                o.worldPosition = o.worldPosition + o.worldNormal * _offsetIntensity * (noise + wave);
                
                o.clipPosition = mul(UNITY_MATRIX_VP, half4(o.worldPosition, 1));
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                half noise = Triplanar(_burstTex, i.triplanarCoord2, i.worldNormal);   
                half dist = length(i.worldPosition - _epicenter.xyz);
                half wave = SmoothSemiWave(dist, _burstWidth, _waveHeight, (_waveRadius - _burstDelay));
                half disapearFactor = 1 - wave * noise;

                half alpha = step(disapearFactor, wave);
                clip (alpha - 1);
                
                // LIGHT
                Light light0 = GetMainLight();
                half3 normalDir = normalize(i.worldNormal);
                half NdotL = saturate(dot(normalDir, light0.direction));
                half specularLobe = Levels(NdotL, _specularLengthLevels);
                
                half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPosition);
                half NdotV = dot(normalDir, viewDir);
                half specularRim = 1 - (2 * abs(Levels(NdotV, _specularLengthLevels) - .5));
                half specularRim2 = 1 - (2 * abs(Levels(NdotV, _rimLevels) - .5));
                
                specularLobe = saturate(step(.2, specularLobe * specularRim));
                
                return half4((_mainColor.xyz + specularLobe * 0.1) * saturate(specularRim2), 1);
            }
            ENDHLSL
        }
    }
}

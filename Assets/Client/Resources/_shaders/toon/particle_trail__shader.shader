﻿Shader "JacaShaders/Cartoon/ParticleTrail"
{
    Properties
    {
        _MainTex ("Noise", 2D) = "white" {}
        _rampTex ("Ramp", 2D) = "white" {}
        [HideInInspector]_rampBaseTex ("Ramp Base", 2D) = "white" {}

        _threshold ("Threshold", range (0,1)) = 0.5
        _noiseBalance ("Noise Spread", range (0, 1)) = 0.5

        // Outline
        _outlineColor ("Outline Color", Color) =  (0,0,0,0)
        _outlineSize ("Outline Size", range (0,1)) = .6
        
  
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            Tags { "LightMode"="UniversalForward" }

            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            #include "Assets/Client/Resources/_shaders/libraries/jaca_shader_lib.hlsl"
            
            uniform sampler2D _MainTex;
            uniform half4 _MainTex_ST;
            uniform sampler2D _rampTex;
            uniform half4 _rampTex_ST;
            uniform sampler2D _rampBaseTex;
            uniform half4 _rampBaseTex_ST;
            uniform half _threshold;
            uniform half _noiseBalance;
            uniform half4 _outlineColor;
            uniform half _outlineSize;

            struct appdata
            {
                float4 objectPosition : POSITION;
                float2 uv : TEXCOORD0;
                float4 vertexColor : COLOR;

            };

            struct v2f
            {
                half2 uv : TEXCOORD0;
                half4 uv2 : TEXCOORD1;
                half4 vertexColor : COLOR;
                half4 clipPosition : SV_POSITION;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                o.uv           = v.uv * _MainTex_ST.xy + _MainTex_ST.zw * _Time.y;
                o.uv2.xy       = v.uv * _rampTex_ST.xy + _rampTex_ST.zw * _Time.y;
                o.uv2.zw       = v.uv * _rampBaseTex_ST.xy + _rampBaseTex_ST.zw * _Time.y;
                o.clipPosition = mul(UNITY_MATRIX_MVP, v.objectPosition); 

                o.vertexColor = v.vertexColor;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Vertex
                half ramp = i.vertexColor.w * tex2D(_rampTex, i.uv2.xy).w;
                ramp *= tex2D(_rampBaseTex, i.uv2.zw).w;
                half noise = lerp(ramp, tex2D(_MainTex, i.uv).w, _noiseBalance);
                half outlineFactor = step(_outlineSize, noise);
                half3 color = lerp(_outlineColor.xyz, i.vertexColor.xyz, outlineFactor);
                half alpha = step(_threshold, noise);
                clip(alpha - .5);
                return half4(color, 1);
            }
            ENDHLSL
        }
    }
}

#ifndef JACA_LIB
#define JACA_LIB

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

#define TAU 6.28318530717
#define PI 3.14159265359

float InverseLerp(float initial_point, float final_point, float value_point)
{
    return (value_point - initial_point) / (final_point - initial_point);
}

float InverseLerpClamped(float initial_point, float final_point, float value_point) // Linear smooth step clamped 0-1
{
    return saturate((value_point - initial_point) / (final_point - initial_point));
}

float RadialWave(float2 uv, float amount, float speed)
{
    float2 center = uv * 2 - 1;
    float radius = length(center);
    return cos((radius - _Time.y * speed) * TAU * amount) * 0.5 + 0.5;
}

float RadialWaveWithGradient(float2 uv, float amount, float speed)
{
    float2 center = uv * 2 - 1;
    float radius = length(center);
    float wave = cos((radius - _Time.y * speed) * TAU * amount) * 0.5 + 0.5;
    return saturate(wave * (1 - radius));
}

half Triplanar(sampler2D noise, half3 world_position, half3 world_normal)
{
    half noiseXY = tex2D(noise, world_position.xy).w;
    half noiseXZ = tex2D(noise, world_position.xz).w;
    half noiseYZ = tex2D(noise, world_position.yz).w;
    half factorXY = abs(world_normal.z);
    half factorXZ = abs(world_normal.y);
    half factorYZ = abs(world_normal.x);
    half noiseFinal = factorXY * noiseXY + factorXZ * noiseXZ + factorYZ * noiseYZ;
    return noiseFinal;
}

half TriplanarLod(sampler2D noise, half3 world_position, half3 world_normal, half mip_level)
{
    half4 triplanar_coord = half4(world_position, mip_level);
    half noiseXY = tex2Dlod(noise, triplanar_coord.xyww).w;
    half noiseXZ = tex2Dlod(noise, triplanar_coord.xzww).w;
    half noiseYZ = tex2Dlod(noise, triplanar_coord.yzww).w;
    half factorXY = abs(world_normal.z);
    half factorXZ = abs(world_normal.y);
    half factorYZ = abs(world_normal.x);
    half noiseFinal = factorXY * noiseXY + factorXZ * noiseXZ + factorYZ * noiseYZ;
    return noiseFinal;
}

half Levels(half x, half4 levels)
{
    return saturate((x - levels.x) * levels.y) * levels.w + levels.z;
}

half Posterize(half x, half steps)
{
    return round(x * steps) / steps;
}

half Wave(half x, half width, half height, half offset)
{
    return max(0, 1 - abs(x + width * 0.5 - offset) / (width * 0.5)) * height;
}

half SmoothWave(half x, half width, half height, half offset)
{
    half wave = max(0, 1 - abs(x + width * 0.5 - offset) / (width * 0.5));
    half smoothWave = (3 * wave * wave - 2 * wave * wave * wave) * height;
    return smoothWave;
}

half SmoothSemiWave(half x, half width, half height, half offset)
{
    half wave = max(0, min(1, (x + width * 0.5 - offset) / (width * 0.5)));
    half smoothWave = (3 * wave * wave - 2 * wave * wave * wave) * height;
    return smoothWave;
}

half2 PlaneProjectedUV(half4 objectPosition, half3 worldSpaceView)
{
    half3 planeNormal = mul(transpose(UNITY_MATRIX_IT_MV), half4(0, 0, -1, 0)).xyz;
    half3 objectSpaceView = normalize(mul(unity_WorldToObject, half4(worldSpaceView, 0)).xyz);
    half NdotO = dot(planeNormal, objectPosition.xyz);
    half NdotV = dot(planeNormal, objectSpaceView);
    half3 projectedPosition = (objectPosition.xyz - objectSpaceView * NdotO / NdotV);
    half3 planeU = normalize(cross(half3(0, 1, 0), planeNormal));
    half3 planeV = cross(planeNormal, planeU);

    return half2(dot(projectedPosition, planeU), dot(projectedPosition, planeV));
}

float2 NormalProjectedUV(float2 uv, float3 normal, float3 view, float dist, float3 tan, float3 bitan)
{
    float3 NxV = cross(normal, view);
    float sin = length(NxV);
    float cos = dot(normal, view);
    float offset = (sin / cos) * dist;
    float3 k = cross(normal, NxV);
    k = normalize(k) * offset;
    float2 tangetSpaceK = float2(dot(tan, k), dot(bitan, k));
    return uv + tangetSpaceK;
}

float SampleLinearDepth(float2 screenPos)
{
    float depth = SampleSceneDepth(screenPos);
    depth = LinearEyeDepth(depth, _ZBufferParams);
    return depth;
}

half LinearSin(half x)
{
    return frac(x * 0.159154);
}

float Noise(half2 coords)
{
    return frac(LinearSin(dot(coords, half2(12.9898, 4.1414))) * 43758.5453);
}
#endif

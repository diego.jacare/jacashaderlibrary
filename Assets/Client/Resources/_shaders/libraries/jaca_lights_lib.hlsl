﻿#ifndef JACA_LIGHTS_LIB
#define JACA_LIGHTS_LIB
    
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

    struct BRDFInfo
    {
        float3 albedo;
        float roughness; 
        float metallic; 
        float f0;
    }; 

    void GetBRDFInfo(float3 albedo, float roughness, float metallic, float f0, out BRDFInfo brdfData)
    {
        brdfData.albedo = albedo;
        brdfData.roughness = roughness;
        brdfData.metallic = metallic;
        brdfData.f0 = f0;
	}

    struct GeometryData
    {
        float3 normal;
        float3 view; 
        float3 reflection;
        float NdotV;
        float fresnel;
    };
    
    void GetGeometryData(float3 normal, float3 worldPosition, out GeometryData geometryData )
    {
        geometryData.normal = normalize(normal);
        geometryData.view = normalize(_WorldSpaceCameraPos - worldPosition);
        geometryData.reflection = reflect(- geometryData.view, geometryData.normal);
        geometryData.NdotV = saturate(dot(geometryData.normal,  geometryData.view));
        geometryData.fresnel = 1 - geometryData.NdotV;
        geometryData.fresnel *= geometryData.fresnel;
        geometryData.fresnel *= geometryData.fresnel;
	}

      struct CustomLight
    {
        float3 direction;
        float3 color;
        float3 halfDirection;
        float NdotL;
        float NdotH;
        float fresnel;
        float shadowAttenuation;
        float3 irradianceMultiplier;
    };

    CustomLight GetCustomMainLight(GeometryData geometryData)
    {
         CustomLight customLight;
         customLight.direction = _MainLightPosition.xyz;
         customLight.color = _MainLightColor.rgb;
         customLight.halfDirection = normalize(geometryData.view + customLight.direction);
         customLight.NdotL = saturate(dot(geometryData.normal, customLight.direction));
         customLight.NdotH = saturate(dot(geometryData.normal, customLight.halfDirection));
         customLight.fresnel = 1 - customLight.NdotL;
         customLight.fresnel *= customLight.fresnel;
         customLight.fresnel *= customLight.fresnel;
         customLight.shadowAttenuation = 1;
         customLight.irradianceMultiplier = customLight.color * customLight.NdotL * customLight.shadowAttenuation;
         return customLight;
	}

    struct GlobalIlluminationData
    {
    };

    GlobalIlluminationData GetGlobalIlluminationData(GeometryData geometryData)
    {
         GlobalIlluminationData globalIlluminationData;
         return globalIlluminationData;
	}
   
   float3 FresnelTerm(float NdotV, float metallic, float3 albedo, float f0)
    {
        float curve = 1 - NdotV; 
        curve *= curve;
        curve *= curve;
        float3 composedF0 = lerp(float3(f0,f0,f0), albedo, metallic);
        return composedF0 + curve * (1 - composedF0);
    }
    
    float FresnelDielectricTerm(float NdotV, float f0)
    {
        float curve = 1 - NdotV; 
        curve *= curve;
        curve *= curve;
        return f0 + curve * (1 - f0);
    }

    float GeometryTerm(float NdotX, float roughness)
    {
        float roughness2 = roughness * roughness;
        float NdotX2 =  NdotX * NdotX; 
        return 2/(1 + sqrt(1 + roughness2 * ((1 - NdotX2)/NdotX2)));
	}

    float DistributionTerm(float NdotH, float roughness)
    {
        float roughness2 = roughness * roughness;
        float div = (roughness2 - 1) * NdotH * NdotH + 1;
        div *= div;
        return roughness2 / div;
    }

    float3 CookTorranceBRDF(BRDFInfo brdfData, float NdotV, float NdotL, float NdotH)
    {
        // Terms
        float3 F = FresnelTerm(NdotV, brdfData.metallic, brdfData.albedo, brdfData.f0);
        float G = GeometryTerm(NdotV, brdfData.roughness) * GeometryTerm(NdotL, brdfData.roughness);
        float D = DistributionTerm(NdotH, brdfData.roughness);
        float3 fLight =  1 - FresnelDielectricTerm(NdotL, brdfData.f0);

        // BRDFs
        float3 cookTorrance =  F * G * D / (4 * NdotL * NdotV);
        float3 diffuse = brdfData.albedo * fLight * (1 - brdfData.metallic);

        return cookTorrance + diffuse;
	}

    float3 RenderLight(BRDFInfo brdfData,GeometryData geometryData,CustomLight customLight)
    {
        return CookTorranceBRDF(brdfData, geometryData.NdotV, customLight.NdotL, customLight.NdotH) * customLight.irradianceMultiplier;
	}

    // TODO: Implement
    float3 RenderGlobalIllumination(BRDFInfo brdfData,GeometryData geometryData,GlobalIlluminationData globalIlluminationData)
    {
        return float3(0,0,0);
	}

#endif
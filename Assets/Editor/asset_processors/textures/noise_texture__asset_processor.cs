using UnityEditor;
using UnityEditor.Presets;

public class noise_texture__asset_processor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        if (assetPath.Contains("Assets/Client/Resources/_textures/vfx") && assetPath.Contains("noise"))
        {
            TextureImporter textureImporter = (TextureImporter)assetImporter;
            Preset texturePreset = AssetDatabase.LoadAssetAtPath<Preset>("Assets/Client/Resources/_textures/_common/noise_texture__import_setting.preset");
            if (texturePreset.CanBeAppliedTo(textureImporter))
            {
                texturePreset.ApplyTo(textureImporter);
            }
        }
    }
}


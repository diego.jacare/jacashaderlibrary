using UnityEditor;
using UnityEditor.Presets;

public class cubemap_texture__asset_processor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        if (assetPath.Contains("Assets/Client/Resources/_textures/cubemaps") && assetPath.Contains("cube"))
        {
            TextureImporter textureImporter = (TextureImporter)assetImporter;
            Preset texturePreset = AssetDatabase.LoadAssetAtPath<Preset>("Assets/Client/Resources/_textures/_common/cubemap__import_setting.preset");
            if (texturePreset.CanBeAppliedTo(textureImporter))
            {
                texturePreset.ApplyTo(textureImporter);
            }
        }
    }
}


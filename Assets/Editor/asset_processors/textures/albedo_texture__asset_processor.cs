using UnityEditor;
using UnityEditor.Presets;

public class albedo_texture__asset_processor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        if (assetPath.Contains("Assets/Client/Resources/_textures/objects") && assetPath.Contains("albedo"))
        {
            TextureImporter textureImporter = (TextureImporter)assetImporter;
            Preset texturePreset = AssetDatabase.LoadAssetAtPath<Preset>("Assets/Client/Resources/_textures/_common/albedo_texture__import_setting.preset");
            if (texturePreset.CanBeAppliedTo(textureImporter))
            {
                texturePreset.ApplyTo(textureImporter);
            }
        }
    }
}


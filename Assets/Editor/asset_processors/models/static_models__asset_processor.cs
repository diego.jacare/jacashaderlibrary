using UnityEditor;
using UnityEditor.Presets;

public class static_models__asset_processor : AssetPostprocessor
{
    void OnPreprocessModel()
    {
        if (assetPath.Contains("Assets/Client/Resources/_models"))
        {
            ModelImporter modelImporter = (ModelImporter)assetImporter;
            Preset modelPreset = AssetDatabase.LoadAssetAtPath<Preset>("Assets/Client/Resources/_models/_common/static_model__import_settings.preset");
            if (modelPreset.CanBeAppliedTo(modelImporter))
            {
                modelPreset.ApplyTo(modelImporter);
            }
        }
    }
}

